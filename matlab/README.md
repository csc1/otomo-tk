The inverse radon construction with MATLAB.

* loadimages.m -- loads the images, clips and possibly negates
* reconstruction.m -- performs the inverse radon construction
* export.m -- saves the reconstruction as raw file
              to be visualized with VTK (see raycast)
