% Exports the reconstructed data as raw file
% Can be visualized with raycast/raycast.py
%
% Alternatively you can use the MATLAB builtin volumeViewer()
%
% This file is part of otomo-tk
% Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
% See LICENSE file.

r = rec2(:);
r = r * 255;
file = fopen( 'export.raw' , 'w' );
fwrite( file, r );
fclose( file );
size( rec2 ) - 1
