% Performs the inverse radon reconstruction
%
% This file is part of otomo-tk
% Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
% See LICENSE file.

% angle of rotation
theta = 2 * 180 / ( size( images, 3 )  );

% radon transform
clearvars rec;

for i = 1 : size( images, 1 )
    i
    rec( : , : , i ) = ...
        iradon( squeeze( images( i, :, : ) ), theta, 'linear', 'ram-lak' );
end

rec2 = rec;
rec2( rec2 < 0 ) = 0;
rec2 = rec2 / max( rec2(:) );

figure, imshow3D( rec2 );