The inverse radon construction with Octave.

This is basically the same as the matlab version only
with some adaptions to slight incompatiblities.

Note that as of 2020 Octave's iradon() is much slower than MATLABs.

* loadimages.m -- loads the images, clips and possibly negates
* reconstruction.m -- performs the inverse radon construction
* export.m -- saves the reconstruction as raw file
              to be visualized with VTK (see raycast)
