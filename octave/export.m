% Exports the reconstructed data as raw file
% Can be visualized with raycast/raycast.py
%
% This file is part of otomo-tk
% Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
% See LICENSE file.

file = fopen( 'export.raw', 'w' );
fwrite( file,r * 255 );
fclose( file );
size( r ) - 1
