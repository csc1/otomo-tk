% Loads the images.
%
% This file is part of otomo-tk
% Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
% See LICENSE file.

clear;
prefix = 'output/';
imagefiles = dir( [ prefix '*.jpg' ] );
nfiles = length( imagefiles );

% if > 1 skips skip-1 images
skip = 1;

% reads one image to get size info
image = imread( [ prefix '0001.jpg' ] );

% y center of clip = height - value from arti
yc = size( image, 1 ) - 1597;
% xy center of clip
xc = 2345;
% width and height of the clip
w = 1287;
h = 619;

% precreates the image matrix
images = zeros(  floor(yc + h/2) - floor(yc - h/2)+1, floor(xc + w/2) - floor(xc - w/2)+1, nfiles/skip );

for i = 1 : nfiles / skip
  printf( "%d / %d\n", i, nfiles / skip );
  fflush( stdout );
  currentfilename = imagefiles( i * skip ).name;
  image = imread( [ prefix currentfilename ] );
   % in case of of rgb images uncomment
   %image = rgb2gray( image );
  image = single( image );
  image = image( floor(yc - h/2) : floor(yc + h/2), floor(xc - w/2) : floor(xc + w/2) );

  images( :, :, i ) = image;
end

% normalizes the images
images = images / max( images( : ) );

% negates the images
images = 1 - images;

% shows the result
imshowSlices( images );
clear currentfilename i image imagefiles nfiles prefix skip;
