% Helper function to show image slices
%
% Very heavily modified version of ../matlab/imshow3D compatible
% with octave
%
% Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
% imshow3D: Copyright (c) 2018, Maysam Shahedi
% imshow3D license applies

function imshowSlices( img )
global v;
v = { };
v.sno = size( img, 3 );
v.slide = 1;
v.img = img;

axes( 'position', [0,0.2,1,0.8] ), imshow( v.img( :, :, v.slide ) )

FigPos = get( gcf, 'Position' );
S_Pos = [50 45 uint16(FigPos(3)-100)+1 20];
Stxt_Pos = [50 65 uint16(FigPos(3)-100)+1 15];
v.shand = uicontrol('Style', 'slider','Min',1,'Max',v.sno,'Value',v.slide,'SliderStep',[1/(v.sno-1) 10/(v.sno-1)],'Position', S_Pos,'Callback', {@SliceSlider});
v.stxthand = uicontrol('Style', 'text','Position', Stxt_Pos,'String',sprintf('Slice# %d / %d',v.slide, v.sno), 'BackgroundColor', [0.8 0.8 0.8], 'FontSize', 9);
end

function SliceSlider( hObj, event )
    global v;
    v.slide = round( get( hObj, 'Value' ) );
    set( get( gca, 'children' ), 'cdata', v.img( :, :, v.slide ) )
    set( v.stxthand, 'String', sprintf('Slice# %d / %d', v.slide, v.sno));
end
