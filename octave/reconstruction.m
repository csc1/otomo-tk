pkg load image

% angle of rotation
theta = 2 * 180 / ( size( images, 3 )  );

% radon transform
clear reconstruction;

for i = 1 : size( images, 1 )
  printf( "%d / %d\n", i, size( images, 1 ) ); fflush( stdout );
  reconstruction( : , : , i ) = ...
    iradon( squeeze( images( i, :, : ) ), theta, 'linear', 'ram-lak' );
end


% depiction
r = reconstruction;
r = r - min(r(:));
r = r / max(r(:));
r( isnan( r ) ) = 0;

figure, imshowSlices( r * 255 );
clear i theta;