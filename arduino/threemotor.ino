/*
| Lets the stepper motor be controlled by serial usb connection.
|
| This is for the 3 motor build, it has two modes:
| * "straight" only turns motor 1 and thus behaves exactly like a 1 motor build.
| * "roller" where it moves motor 2 and 3 to simulate axis instability.
|
| It contains a protection logic in the Arduino to never move motor 2 and 3
| beyond set limits thus not to destroying the assembly.
|
| The communication with the controlling pc is handled by a single character command
| protocol whose specifictions can be read in the  handleChar( ) function.
|
| This file is part of otomo-tk
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/

// after 30 seconds the drivers shall be put to sleep
const unsigned long autoDisableWait = 30000;

// never moves the angular motors beyond this limit (pos and negative)
// as this might destroy the assembly.
const int ANGLE_MOT_LIMIT = 250;

unsigned long autoDisableAt = 0;
bool enabled = false;

// remember motor positions and return when disabling
int motor1pos = 0;
int motor2pos = 0;
int motor3pos = 0;

// current position in current program
int prgpos = 0;

// current program
// 'r' ... roller
// 's' ... straight (default)
char prg = 's';

// direction: HIGH -> ccw, LOW -> cw
int8_t motor1dir = LOW;
int8_t motor2dir = LOW;
int8_t motor3dir = LOW;

void setup( )
{
	pinMode(  2, OUTPUT ); // motor all sleep
	pinMode(  3, OUTPUT ); // motor 1 dir
	pinMode(  4, OUTPUT ); // motor 1 step
	pinMode(  5, OUTPUT ); // motor 2 dir
	pinMode(  6, OUTPUT ); // motor 2 step
	pinMode(  7, OUTPUT ); // motor 3 dir
	pinMode(  8, OUTPUT ); // motor 3 step

	digitalWrite( 2, HIGH ); // disable all motors
	digitalWrite( 4,  LOW ); // no step, motor 1
	digitalWrite( 6,  LOW ); // no step, motor 2
	digitalWrite( 8,  LOW ); // no step, motor 3

	digitalWrite( 3, motor1dir );
	digitalWrite( 5, motor2dir );
	digitalWrite( 7, motor3dir );

	Serial.begin(19200);
}

/*
| Enables all the motor drivers.
*/
void enable( )
{
	if( !enabled )
	{
		digitalWrite( 2, LOW );
		delay( 1 );
		enabled = true;
	}
	autoDisableAt = millis( ) + autoDisableWait;
}

/*
| Returns all 3 motors into their starting position.
| Finally puts the motor drivers to sleep.
*/
void disable( )
{
	if( !enabled ) return;

	// return the motors into their zero position
	motor1dir = motor1pos > 0 ? HIGH : LOW;
	motor2dir = motor2pos > 0 ? HIGH : LOW;
	motor3dir = motor3pos > 0 ? HIGH : LOW;

	digitalWrite( 3, motor1dir );
	digitalWrite( 5, motor2dir );
	digitalWrite( 7, motor3dir );

	while( motor1pos || motor2pos || motor3pos )
	{
		if( motor1pos ) digitalWrite( 4, HIGH );
		if( motor2pos ) digitalWrite( 6, HIGH );
		if( motor3pos ) digitalWrite( 8, HIGH );
		delayMicroseconds( 500 );
		if( motor1pos ) digitalWrite( 4, LOW );
		if( motor2pos ) digitalWrite( 6, LOW );
		if( motor3pos ) digitalWrite( 8, LOW );
		if( motor1pos ) motor1pos = motor1pos + ( motor1dir ? -1 : 1 );
		if( motor2pos ) motor2pos = motor2pos + ( motor2dir ? -1 : 1 );
		if( motor3pos ) motor3pos = motor3pos + ( motor3dir ? -1 : 1 );
	}

	if( motor1dir ) { digitalWrite( 3, LOW ); motor1dir = LOW; }
	if( motor2dir ) { digitalWrite( 5, LOW ); motor2dir = LOW; }
	if( motor3dir ) { digitalWrite( 7, LOW ); motor3dir = LOW; }

	prgpos = 0;
	// finally puts drivers to sleep.
	digitalWrite( 2, HIGH );
	enabled = false;
}

/*
| Made a step with motor 1.
*/
void motor1didstep( )
{
	motor1pos = motor1pos + ( motor1dir ? -1 : 1 );
	motor1pos = ( motor1pos + 1600 ) % 3200 - 1600;
}

/*
| Made a step with motor 2.
*/
void motor2didstep( )
{
	motor2pos = motor2pos + ( motor2dir ? -1 : 1 );
	motor2pos = ( motor2pos + 1600 ) % 3200 - 1600;
}

/*
| Made a step with motor 3.
*/
void motor3didstep( )
{
	motor3pos = motor3pos + ( motor3dir ? -1 : 1 );
	motor3pos = ( motor3pos + 1600 ) % 3200 - 1600;
}

/*
| Returns HIGH if shortest way from pos to target is CCW.
| Returns LOW if shortest way is CW.
*/
int8_t getmotordir( int pos, int target )
{
	// difference
	int diff  = target - pos;
	if( diff == 0 ) return LOW;
	if( diff < -1600 ) return LOW;
	if( diff > 1600 ) return HIGH;
	if( diff >= 0 ) return LOW;
	return HIGH;
}

/*
| Adjusts the motors to fit current roller program position.
*/
bool adjustRoller( )
{
	// effective roller position
	int erp = prgpos * 4;
	if( erp  > 3200 ) return false;

	// positions the motors ought to be moved to
	int motor1target = (( erp + 1600 ) % 3200 ) - 1600;
	int motor2target = sin( erp * 2.0f * PI / 3200.0f ) * 200.0f;
	int motor3target = cos( erp * 2.0f * PI / 3200.0f ) * 200.0f;

	motor1dir = getmotordir( motor1pos, motor1target );
	digitalWrite( 3, motor1dir );

	motor2dir = getmotordir( motor2pos, motor2target );
	digitalWrite( 5, motor2dir );

	motor3dir = getmotordir( motor3pos, motor3target );
	digitalWrite( 7, motor3dir );

	Serial.write( 'p' );
	Serial.print( prgpos );
	Serial.write( "\r\n" );

	if(
		motor2target >= ANGLE_MOT_LIMIT || motor2target < -ANGLE_MOT_LIMIT
		|| motor3target >= ANGLE_MOT_LIMIT || motor3target < -ANGLE_MOT_LIMIT
	)
	{
		Serial.write( "FAILURE: ANGULAR MOTOR LIMIT\r\n");
		return false;
	}

	while( motor1pos != motor1target || motor2pos != motor2target || motor3pos != motor3target )
	{
		if( motor1pos != motor1target ) digitalWrite( 4, HIGH );
		if( motor2pos != motor2target ) digitalWrite( 6, HIGH );
		if( motor3pos != motor3target ) digitalWrite( 8, HIGH );
		delayMicroseconds( 500 );
		if( motor1pos != motor1target) { digitalWrite( 4, LOW ); motor1didstep( ); }
		if( motor2pos != motor2target) { digitalWrite( 6, LOW ); motor2didstep( ); }
		if( motor3pos != motor3target) { digitalWrite( 8, LOW ); motor3didstep( ); }
	}
	return true;
}

/*
| Adjusts the motors to fit current straight program position.
*/
bool adjustStraight( )
{
	// effective roller position
	int erp = prgpos * 4;
	if( erp  > 3200 ) return false;

	// positions the motors ought to be moved to
	int motor1target = (( erp + 1600 ) % 3200 ) - 1600;

	motor1dir = getmotordir( motor1pos, motor1target );
	digitalWrite( 3, motor1dir );

	Serial.write( 'p' );
	Serial.print( prgpos );
	Serial.write( "\r\n" );

	while( motor1pos != motor1target )
	{
		if( motor1pos != motor1target ) digitalWrite( 4, HIGH );
		delayMicroseconds( 500 );
		if( motor1pos != motor1target) { digitalWrite( 4, LOW ); motor1didstep( ); }
	}

	return true;
}

/*
| Adjusts the motors to fit current program.
*/
bool adjust( )
{
	switch( prg )
	{
		case 'r' : return adjustRoller( );
		case 's' : return adjustStraight( );
		default : return false;
	}
}

/*
| Does a roller step. Returns false when done.
*/
bool rollerStep( )
{
	prgpos++;
	return adjust( );
}

/*
| Writes 'str' to serial.
*/
void swrite( const char * str )
{
	Serial.write( str );
	Serial.write( "\r\n" );
}

/*
| Handles a command on the serial connection.
*/
void handleChar( int c )
{
	switch( c )
	{
		// hello test
		case '.' : swrite( "hello (threemotor v1.0)" ); return;

		// enable
		case 'e' : enable( ); swrite( "enabled" ); return;

		// disable
		case 'd' : disable( ); swrite( "disabled" ); return;

		// select program roller
		case 'r' :
			if( enabled ) { swrite( "already enabled" ); return; }
			swrite( "roller program selected" );
			prg = 'r';
			return;

		// select program roller
		case 's' :
			if( enabled ) { swrite( "already enabled" ); return; }
			swrite( "straight program selected" );
			prg = 's';
			return;

		// go to position 0
		case '0' :
			if( !enabled ) { swrite( "disabled" ); return; }
			prgpos = 0;
			adjust( );
			return;

		// 1 step
		case '1' :
			if( !enabled ) { swrite( "disabled" ); return; }
			rollerStep( );
			return;

		// full circle
		case 'c' :
			if( !enabled ) { swrite( "disabled" ); return; }
			while( rollerStep( ) );
			swrite( "done" );
			return;
	}
}

void loop( )
{
	if( Serial.available() )
	{
		// auto disable
		if( autoDisableAt ) autoDisableAt = millis( ) + autoDisableWait;
		int c = Serial.read( );
		handleChar( c );
	}
	else if( autoDisableAt && millis( ) > autoDisableAt )
	{
		swrite( "auto disabled" );
		disable( );
		autoDisableAt = 0;
	}
}
