/*
| Lets the stepper motor be controlled by serial usb connection.
|
| This is for the 1 motor build.
|
| The communication with the controlling pc is handled by a single character command
| protocol whose specifictions can be read in the  handleChar( ) function.
|
| This file is part of otomo-tk
| Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
| See LICENSE file.
*/

// after 30 seconds the drivers shall be put to sleep
const unsigned long autoDisableWait = 30000;

unsigned long autoDisableAt = 0;
bool enabled = false;

// remember motor positions and return when disabling
int motor1pos = 0;

// current position in current program
int prgpos = 0;

// direction: HIGH -> ccw, LOW -> cw
int8_t motor1dir = LOW;

void setup()
{
	pinMode(  2, OUTPUT ); // motor all sleep
	pinMode(  3, OUTPUT ); // motor 1 dir
	pinMode(  4, OUTPUT ); // motor 1 step

	digitalWrite( 2, HIGH ); // disable motor
	digitalWrite( 3, motor1dir );
	digitalWrite( 4,  LOW ); // no step

	Serial.begin(19200);
}

/*
| Enables all the motor drivers.
*/
void enable( )
{
	if( !enabled )
	{
		digitalWrite( 2, LOW );
		delay( 1 );
		enabled = true;
	}
	autoDisableAt = millis( ) + autoDisableWait;
}

/*
| Returns the motor into it's starting position.
| Finally puts the motor driver to sleep.
*/
void disable( )
{
	if( !enabled ) return;

	// return the motors into their zero position
	motor1dir = motor1pos > 0 ? HIGH : LOW;
	digitalWrite( 3, motor1dir );

	while( motor1pos )
	{
		digitalWrite( 4, HIGH );
		delayMicroseconds( 500 );
		digitalWrite( 4, LOW );
		motor1pos = motor1pos + ( motor1dir ? -1 : 1 );
	}

	if( motor1dir ) { digitalWrite( 3, LOW ); motor1dir = LOW; }
	prgpos = 0;

	// finally puts the driver to sleep
	digitalWrite( 2, HIGH );
	enabled = false;
}

/*
| Made a step.
*/
void motor1didstep( )
{
	motor1pos = motor1pos + ( motor1dir ? -1 : 1 );
	motor1pos = ( motor1pos + 1600 ) % 3200 - 1600;
}


/*
| Returns HIGH if shortest way from pos to target is CCW.
| Returns LOW if shortest way is CW.
*/
int8_t getmotordir( int pos, int target )
{
	// difference
	int diff  = target - pos;
	if( diff == 0 ) return LOW;
	if( diff < -1600 ) return LOW;
	if( diff > 1600 ) return HIGH;
	if( diff >= 0 ) return LOW;
	return HIGH;
}


/*
| Adjusts the motor to fit current roller program position.
*/
bool adjust( )
{
	// effective roller position
	int erp = prgpos * 4;
	if( erp  > 3200 ) return false;

	// position the motor ought to be moved to
	int motor1target = (( erp + 1600 ) % 3200 ) - 1600;

	motor1dir = getmotordir( motor1pos, motor1target );
	digitalWrite( 3, motor1dir );

	Serial.write( 'p' );
	Serial.print( prgpos );
	Serial.write( "\r\n" );

	while( motor1pos != motor1target )
	{
		if( motor1pos != motor1target ) digitalWrite( 4, HIGH );
		delayMicroseconds( 500 );
		if( motor1pos != motor1target) { digitalWrite( 4, LOW ); motor1didstep( ); }
	}
	return true;
}


/*
| Does a step. Returns false when done.
*/
bool step( )
{
	prgpos++;
	return adjust( );
}

/*
| Writes str to serial.
*/
void swrite( const char * str )
{
	Serial.write( str );
	Serial.write( "\r\n" );
}

/*
| Handles a command on the serial connection.
*/
void handleChar( int c )
{
	switch( c )
	{
		// hello test
		case '.' : swrite( "hello (onemotor v1.0)" ); return;

		// enable
		case 'e' : enable( ); swrite( "enabled" ); return;

		// disable
		case 'd' : disable( ); swrite( "disabled" ); return;

		// go to position 0
		case '0' :
			if( !enabled ) { swrite( "disabled" ); return; }
			prgpos = 0;
			adjust( );
			return;

		// 1 step
		case '1' :
			if( !enabled ) { swrite( "disabled" ); return; }
			step( );
			return;

		// full circle
		case 'c' :
			if( !enabled ) { swrite( "disabled" ); return; }
			while( step( ) );
			swrite( "done" );
			return;
	}
}

void loop( )
{
	if( Serial.available() )
	{
		// auto disable
		if( autoDisableAt ) autoDisableAt = millis( ) + autoDisableWait;
		int c = Serial.read( );
		handleChar( c );
	}
	else if( autoDisableAt && millis( ) > autoDisableAt )
	{
		swrite( "auto disabled" );
		disable( );
		autoDisableAt = 0;
	}
}
