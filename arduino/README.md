These are two programs to be ran on an Arduino for a one motor
or three motor build.

Communication with the controlling PC is handled by a simple
one character protocol.

The stepper motors are to be connected via drivers (e.g. A4988) to the Arduino.
