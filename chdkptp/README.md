This is a copy of chdkptp (https://app.assembla.com/wiki/show/chdkptp).

Run `setup-ext-libs.bash` in misc to setup the needed libraries.

Use the GUI to properly configure you camera and note the tv, sv and zoom parameters used
and alter otomoctrl.lua to match these.
