-- this controlls the arduino controlling the motors
-- and the camera making one full recording
--
-- it expects an "output" directory to put the images into.
--
-- This file is part of otomo-tk
-- Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
-- See LICENSE file.

-- camera parameters (change these to adapt)
local tv = '1/120'
local sv = '40'
local zoom = '12'
-- recording parameters
local steps = 200
-- amount of motor steps for a recording step
local stepsize = 4
-- milliseconds between every motor step
local stepSleep = { tv_sec = 0, tv_nsec = 150 * 1000 * 1000 }
-- extra milliseconds before shooting (so the object can settle}
local extraSleep = { tv_sec = 0, tv_nsec = 150 * 1000 * 1000 }


package.cpath = package.cpath .. ';./luaposix/linux/?.so'

local fcntl = require( 'posix.fcntl' )
local unistd = require( 'posix.unistd' )
local stdio = require( 'posix.stdio' )
local termio = require( 'posix.termio' )
local time = require( 'posix.time' )


local serial = assert( fcntl.open( '/dev/ttyACM0', fcntl.O_RDWR + fcntl.O_NONBLOCK + fcntl.O_NOCTTY ) )
local tcattr = assert( termio.tcgetattr( serial ) )

tcattr.cflag = termio.CS8 + termio.CLOCAL + termio.CREAD
tcattr.ispeed = termio.B19200;
tcattr.ospeed = termio.B19200;
tcattr.iflag = 0;
tcattr.oflag = 0;
tcattr.lflag = 0;

assert( termio.tcsetattr( serial, termio.TCSANOW, tcattr ) )
print( 'looking for arduino motor controller...' )

local c
while c ~= ')'
do
	unistd.write( serial, '.' )
	c, c1, c2 = unistd.read( serial, 1 )
	print( c, c1, c2 )
end

print( 'emptying the stream' )
repeat
	c, c1, c2 = unistd.read( serial, 10 )
	print( c, c1, c2 )
	if c == nil then break end
until false

print( 'connected to arduino motor controller!' )

print( 'ensuring cam is in record mode' )
cli:execute( 'rec' );
print( 'focusing' )
cli:execute( 'click("shoot_half")' );
print( 'zooming' )
cli:execute( 'luar set_zoom('..zoom..')' );

print( 'configuring arduino motor controller' )

-- enabling motor
print( 'enabling motor' )
unistd.write( serial, 'e' )

print( 'start!' )
for i = 1, steps
do
	for j = 1, stepsize
	do
		unistd.write( serial, '1' )
		time.nanosleep( stepSleep )
	end
	time.nanosleep( extraSleep )
	local name = ''
	if i < 1000 then name = name .. '0' end
	if i <  100 then name = name .. '0' end
	if i <   10 then name = name .. '0' end
	name = 'output/' .. name .. i
	print( 'shooting:', name )
	cli:execute( 'remoteshoot -tv='..tv..' -sv='..sv..' '..name )
end

print( 'disabling motor' )
unistd.write( serial, 'd' )

print( 'done!' )
