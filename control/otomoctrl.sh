#!/bin/sh
OTOMO_DIR=$(readlink -f "$(dirname "$(readlink -f "$0")")/../")
CHDKPTP_DIR=$OTOMO_DIR/chdkptp
export LD_LIBRARY_PATH="$CHDKPTP_DIR/lib"
## set lua paths, double ; appends default
export LUA_PATH="$CHDKPTP_DIR/lua/?.lua;;"
export LUA_CPATH="$CHDKPTP_DIR/?.so;;"
export GDK_BACKEND=x11
"$CHDKPTP_DIR/chdkptp" "$@" "-c" "-eexec dofile('${OTOMO_DIR}/control/otomoctrl.lua')"
