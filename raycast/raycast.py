#!/usr/bin/env python3
#
# A wrapper for VTK to make a raycast from volume data in a raw file.
#
# This file is part of otomo-tk
# Authors: 2020, Axel Kittenberger (axel.kittenberger@univie.ac.at)
# See LICENSE file.
#

# file to read
filename = './radon.raw';

# dimensions of volume (adapt)
x = 705
y = 705
z = 879

# preset cut and intensity
cut = 10
intensity = 0.6

# set true for stereoscopic 3D output
stereo = False

# set true to run on GPU
gpu = True

#only used if gpu is False, should be aprox. amount of cores
threads = 8

from vtk import vtkRenderer
from vtk import vtkRenderWindow
from vtk import vtkRenderWindowInteractor
from vtk import vtkImageReader2
from vtk import vtkPiecewiseFunction
from vtk import vtkColorTransferFunction
from vtk import vtkVolumeProperty
from vtk import vtkVolume
from vtk import vtkOpenGLGPUVolumeRayCastMapper
from vtk import vtkFixedPointVolumeRayCastMapper
from vtk import vtkInteractorStyleTrackballCamera
from vtk import vtkSliderRepresentation2D
from vtk import vtkSliderWidget

# Create the standard renderer, render window and interactor
ren = vtkRenderer( )
renWin = vtkRenderWindow( )
if stereo: renWin.StereoCapableWindowOn()
renWin.AddRenderer( ren )
iren = vtkRenderWindowInteractor( )
iren.SetRenderWindow( renWin )

reader = vtkImageReader2( )
reader.SetFileName( filename );
reader.SetDataScalarTypeToUnsignedChar( );
reader.SetFileDimensionality( 3 );
reader.SetDataExtent( 0, x, 0, y, 0, z );

# Create transfer mapping scalar value to opacity
opacityTransferFunction = vtkPiecewiseFunction( )

def SetOpacity():
	global intensity, cut
	opacityTransferFunction.RemoveAllPoints()
	opacityTransferFunction.AddPoint(   0, 0.0 )
	opacityTransferFunction.AddPoint( cut, 0.0 )
	opacityTransferFunction.AddPoint( 255, intensity )

SetOpacity()

# Create transfer mapping scalar value to color

volumeProperty = vtkVolumeProperty( )
volumeProperty.SetScalarOpacity( 0, opacityTransferFunction )
volumeProperty.SetInterpolationTypeToLinear( )

if gpu: volumeMapper = vtkOpenGLGPUVolumeRayCastMapper( )
else: volumeMapper = vtkFixedPointVolumeRayCastMapper( )

volumeMapper.SetInputConnection( reader.GetOutputPort( ) )
if not gpu: volumeMapper.SetNumberOfThreads( threads )

volume = vtkVolume( )
volume.SetMapper( volumeMapper )
volume.SetProperty( volumeProperty )

ren.AddVolume( volume )
ren.SetBackground( 1, 1, 1 )
renWin.SetSize( 600, 600 )

def CheckAbort( obj, event ):
#    if obj.GetEventPending( ) != 0:
	obj.SetAbortRender( 1 )

renWin.AddObserver( 'AbortCheckEvent', CheckAbort )

style = vtkInteractorStyleTrackballCamera( )
iren.SetInteractorStyle( style )

iren.Initialize( )
renWin.Render( )

sliderCutRep = vtkSliderRepresentation2D()
sliderCutRep.SetMinimumValue(0.0)
sliderCutRep.SetMaximumValue(255.0)
sliderCutRep.SetValue(cut)
sliderCutRep.GetSliderProperty().SetColor(0,0,0)
sliderCutRep.GetTitleProperty().SetColor(0,0,0)
sliderCutRep.GetLabelProperty().SetColor(0,0,0)
sliderCutRep.GetTubeProperty().SetColor(0,0,0)
sliderCutRep.GetCapProperty().SetColor(0,0,0)
sliderCutRep.GetPoint1Coordinate().SetCoordinateSystemToDisplay()
sliderCutRep.GetPoint2Coordinate().SetCoordinateSystemToDisplay()
sliderCutWidget = vtkSliderWidget()

sliderIntensityRep = vtkSliderRepresentation2D()
sliderIntensityRep.SetMinimumValue(0.0)
sliderIntensityRep.SetMaximumValue(1.0)
sliderIntensityRep.SetValue(intensity)
sliderIntensityRep.GetSliderProperty().SetColor(0,0,0)
sliderIntensityRep.GetTitleProperty().SetColor(0,0,0)
sliderIntensityRep.GetLabelProperty().SetColor(0,0,0)
sliderIntensityRep.GetTubeProperty().SetColor(0,0,0)
sliderIntensityRep.GetCapProperty().SetColor(0,0,0)
sliderIntensityRep.GetPoint1Coordinate().SetCoordinateSystemToDisplay()
sliderIntensityRep.GetPoint2Coordinate().SetCoordinateSystemToDisplay()
sliderIntensityWidget = vtkSliderWidget()

def Callback( obj, ev ):
	global intensity, cut
	intensity = sliderIntensityRep.GetValue()
	cut = round( sliderCutRep.GetValue( ) )
	sliderCutRep.SetValue( cut )
	SetOpacity()

sliderCutWidget.AddObserver('InteractionEvent', Callback)
sliderIntensityWidget.AddObserver('InteractionEvent', Callback)

def Resized( obj, ev ):
	size = renWin.GetSize( )
	sx = size[ 0 ]
	sy = size[ 1 ]
	sliderCutRep.GetPoint1Coordinate().SetValue( 20 , 40 )
	sliderCutRep.GetPoint2Coordinate().SetValue( round( ( sx - 40 ) / 2 + 20 ) , 40 )
	sliderIntensityRep.GetPoint1Coordinate().SetValue( round( ( sx - 40 ) / 2 + 20 ), 40 )
	sliderIntensityRep.GetPoint2Coordinate().SetValue( sx - 20, 40 )


renWin.AddObserver( 'ModifiedEvent', Resized )
Resized( None, None )

sliderCutWidget.SetInteractor( iren )
sliderCutWidget.SetRepresentation( sliderCutRep )
sliderCutWidget.EnabledOn( )

sliderIntensityWidget.SetInteractor( iren )
sliderIntensityWidget.SetRepresentation( sliderIntensityRep )
sliderIntensityWidget.EnabledOn( )

if stereo: renWin.SetStereoTypeToRedBlue

#"Crystal eyes" would be shutter technology
#if stereo: renWin.SetStereoTypeToCrystalEyes

if stereo: renWin.StereoRenderOn( )

renWin.Render( )

iren.Start( )

